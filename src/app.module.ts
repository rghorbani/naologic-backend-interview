import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

import { ProductModule } from './products';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/naologic'),
    ScheduleModule.forRoot(),

    // Modules
    ProductModule,
  ],
})
export class AppModule {}
