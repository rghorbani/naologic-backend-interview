import { Controller, Get, HttpCode, HttpStatus, Post } from '@nestjs/common';

import { ProductService } from '../services';

@Controller('products')
export class ProductsController {
  constructor(
    private readonly productService: ProductService,
  ) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  public async import(): Promise<any> {
    return this.productService.importProducts();
  }
}
