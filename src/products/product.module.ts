import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ProductsController } from './controllers';
import { ImportScheduler } from './schedulers';
import { Product, ProductSchema } from './schemas';
import { ProductService } from './services';

@Module({
  controllers: [
    ProductsController,
  ],
  imports: [
    MongooseModule.forFeature([
      { name: Product.name, schema: ProductSchema },
    ]),
  ],
  providers: [
    // Schedulers
    ImportScheduler,
    // Services
    ProductService,
  ],
})
export class ProductModule {}
