import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { ProductService } from '../services';

@Injectable()
export class ImportScheduler {
  constructor(
    private readonly productService: ProductService,
  ) {}

  @Cron('0 0 * * *')
  async handleImport() {
    console.debug('Called every day at midnight');
    this.productService.importProducts();
  }
}