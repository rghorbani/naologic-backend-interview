import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ProductDocument = Product & Document;

@Schema({ timestamps: true })
export class Product {
  @Prop({ required: true })
  productId: string;

  @Prop({ required: true })
  itemId: string;

  @Prop()
  name: string;

  @Prop()
  description: string;

  @Prop()
  packaging: string;

  @Prop({ type: Number, default: 0 })
  unitPrice: number;

  @Prop({ type: Number, default: 0 })
  quantityOnHand: number;

  @Prop()
  availability: string;

  @Prop()
  vendorId: string;

  @Prop()
  manufacturerId: string;

  @Prop({ required: true })
  docId: string;

  @Prop({ type: [{ id: String, value: String }] })
  options: { id: string, value: string }[];

  @Prop({
    type: [
      {
        id: String,
        available: Boolean,
        attributes: { packaging: String, description: String },
        cost: Number,
        currency: String,
        description: String,
        manufacturerItemCode: String,
        manufacturerItemId: String,
        packaging: String,
        price: Number,
        optionName: String,
        optionsPath: String,
        optionItemsPath: String,
        sku: { type: String, unique: true },
        active: Boolean,
        images: [{ fileName: String, cdnLink: String, i: Number, alt: String }],
        itemCode: String,
      },
    ],
  })
  variants: {
    id: string;
    available: boolean;
    attributes: { packaging: string; description: string };
    cost: number;
    currency: string;
    description: string;
    manufacturerItemCode: string;
    manufacturerItemId: string;
    packaging: string;
    price: number;
    optionName: string;
    optionsPath: string;
    optionItemsPath: string;
    sku: string;
    active: boolean;
    images: [{ fileName: string; cdnLink: string; i: number; alt: string }];
    itemCode: string;
  }[];
}

export const ProductSchema = SchemaFactory.createForClass(Product);

// Indexes
ProductSchema.index({ productId: 1 });
ProductSchema.index({ itemId: 1 });
ProductSchema.index({ productId: 1, 'variants.id': 1 });