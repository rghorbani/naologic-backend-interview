import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import mongoose, { Connection, Model } from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';

import { Product, ProductSchema } from '../schemas';
import { ProductService } from './product.service';

describe('ProductService', () => {
  let service: ProductService;
  let productModel: Model<Product>;
  let mongod: MongoMemoryServer;
  let connection: Connection;

  beforeAll(async () => {
    mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();

    connection = await mongoose.createConnection(uri).asPromise();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductService,
        {
          provide: getModelToken(Product.name),
          useValue: connection.model(Product.name, ProductSchema),
        },
      ],
    }).compile();

    service = module.get<ProductService>(ProductService);
    productModel = module.get<Model<Product>>(getModelToken(Product.name));
  });

  afterAll(async () => {
    await connection.close();
    await mongod.stop();
  });

  afterEach(async () => {
    await productModel.deleteMany({});
  });

  describe('importProducts', () => {
    it('should parse and save products correctly', async () => {
      const csvData = [
        {
          ProductID: '10033525',
          ItemID: '10289480',
          ProductName: 'Test Product',
          ProductDescription: 'Test Description',
          PKG: 'Test Packaging',
          UnitPrice: '10.00',
          QuantityOnHand: '100',
          Availability: 'In Stock',
          ManufacturerName: 'Test Manufacturer',
          ManufacturerItemCode: '12345',
          ItemDescription: 'Test Item Description'
        },
      ];

      jest.spyOn(service, 'fetchVendors').mockResolvedValue({});
      jest.spyOn(service, 'fetchManufacturers').mockResolvedValue({});
      jest.spyOn(service, 'enhanceDescriptions').mockResolvedValue(undefined);

      jest.spyOn(service, 'parseCSV').mockResolvedValue(csvData);

      await service.importProducts();

      const products = await productModel.find().exec();

      expect(products.length).toBe(1);
      expect(products[0]).toMatchObject({
        productId: '10033525',
        itemId: '10289480',
        name: 'Test Product',
        description: 'Test Description',
        packaging: 'Test Packaging',
        unitPrice: 10.00,
        quantityOnHand: 100,
        availability: 'In Stock',
        vendorName: 'Test Manufacturer',
        manufacturerName: 'Test Manufacturer',
        docId: expect.any(String),
      });
    });
  });
});
