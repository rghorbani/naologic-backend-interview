import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as csvParser from 'csv-parser';
import { nanoid } from 'nanoid';
import axios from 'axios';
import * as fs from 'fs';

import { Product, ProductDocument } from '../schemas';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
  ) {}

  async importProducts() {
    const products = await this.parseCSV('images40.txt');
    // const products = await this.parseCSV('sample.csv');
    const vendors = await this.fetchVendors();
    const manufacturers = await this.fetchManufacturers();

    for (const product of products) {
      product.vendorId = vendors[product.vendorName]?.id || nanoid();
      product.manufacturerId = manufacturers[product.manufacturerName]?.id || nanoid();
      await this.saveProduct(product);
    }

    await this.enhanceDescriptions();
  }

  async parseCSV(filePath: string): Promise<any[]> {
    const products = [];
    return new Promise((resolve, reject) => {
      fs.createReadStream(filePath)
        .pipe(csvParser({ separator: '\t' }))
        .on('data', (row) => {
          try {
            console.log('Row:', row);
            const formattedRow = this.formatRow(row);
            console.log('Formatted Row:', formattedRow)
            products.push(formattedRow);
          } catch (error) {
            console.error(`Error parsing row: ${error.message} - ${JSON.stringify(row)}`);
          }
        })
        .on('end', () => resolve(this.groupByProductId(products)))
        .on('error', reject);
    });
  }

  formatRow(row): any {
    const toNumber = (value) => {
      const number = parseFloat(value);
      return isNaN(number) ? 0 : number;
    };
  
    if (!row.ProductID || !row.ItemID) {
      throw new Error('Missing required fields: ProductID or ItemID');
    }
  
    // Formatting the product data
    const product = {
      productId: row.ProductID,
      itemId: row.ItemID,
      name: row.ProductName || 'Unnamed Product',
      description: row.ProductDescription || '',
      packaging: row.PKG || '',
      unitPrice: toNumber(row.UnitPrice),
      quantityOnHand: toNumber(row.QuantityOnHand),
      availability: row.Availability || 'Unknown',
      vendorName: row.ManufacturerName || 'Unknown Vendor',
      manufacturerName: row.ManufacturerName || 'Unknown Manufacturer',
      docId: nanoid(),
      options: [
        {
          id: nanoid(),
          name: 'packaging',
          values: [
            {
              id: nanoid(),
              name: row.PKG || 'No Packaging',
              value: row.PKG || 'No Packaging'
            }
          ]
        },
        {
          id: nanoid(),
          name: 'description',
          values: [
            {
              id: nanoid(),
              name: row.ItemDescription || 'No Description',
              value: row.ItemDescription || 'No Description'
            }
          ]
        }
      ],
      variants: [
        {
          id: nanoid(),
          available: true,
          attributes: {
            packaging: row.PKG,
            description: row.ItemDescription
          },
          cost: toNumber(row.UnitPrice),
          currency: 'USD',
          description: row.ItemDescription,
          manufacturerItemCode: row.ManufacturerItemCode,
          manufacturerItemId: row.ManufacturerItemId,
          packaging: row.PKG,
          price: toNumber(row.UnitPrice),
          optionName: `${row.PKG}, ${row.ItemDescription}`,
          optionsPath: 'path1.path2',
          optionItemsPath: 'path3.path4',
          sku: `${row.ManufacturerItemId}${row.ItemID}`,
          active: true,
          images: [
            {
              fileName: '',
              cdnLink: null,
              i: 0,
              alt: null
            }
          ],
          itemCode: row.ManufacturerItemCode
        }
      ]
    };
  
    return product;
  }  

  groupByProductId(rows): any[] {
    const grouped = {};

    rows.forEach((row) => {
      if (!grouped[row.productId]) {
        grouped[row.productId] = { ...row, variants: [] };
      }
      grouped[row.productId].variants.push({
        id: nanoid(),
        available: true,
        attributes: {
          packaging: row.packaging,
          description: row.description
        },
        cost: row.unitPrice,
        currency: 'USD',
        description: row.description,
        manufacturerItemCode: row.manufacturerItemCode,
        manufacturerItemId: row.manufacturerItemId,
        packaging: row.packaging,
        price: row.unitPrice,
        optionName: `${row.packaging}, ${row.description}`,
        optionsPath: 'path1.path2',
        optionItemsPath: 'path3.path4',
        sku: `${row.manufacturerItemId}${row.itemId}`,
        active: true,
        images: [
          {
            fileName: '',
            cdnLink: null,
            i: 0,
            alt: null
          }
        ],
        itemCode: row.manufacturerItemCode
      });
    });

    return Object.values(grouped);
  }

  async fetchVendors() {
    // Fetch vendors
    return {};
  }

  async fetchManufacturers() {
    // Fetch manufacturers
    return {};
  }

  async saveProduct(product) {
    const existingProduct = await this.productModel.findOne({ productId: product.productId }).exec();
    if (existingProduct) {
      await this.productModel.updateOne({ productId: product.productId }, product).exec();
    } else {
      const newProduct = new this.productModel(product);
      await newProduct.save();
    }
  }

  async enhanceDescriptions() {
    const products = await this.productModel.find().limit(10).exec();
    for (const product of products) {
      const prompt = `
      You are an expert in medical sales. Your specialty is medical consumables used by hospitals on a daily basis. Your task to enhance the description of a product based on the information provided.

      Product description: ${product.description}

      New Description:
      `;
      const response = await this.callGPT4(prompt);
      product.description = response.data.choices[0].text.trim();
      await product.save();
    }
  }

  async callGPT4(prompt: string): Promise<any> {
    return axios.post('https://api.openai.com/v1/engines/gpt-4/completions', {
      prompt,
      max_tokens: 100,
    }, {
      headers: {
        Authorization: `Bearer OPENAI_API_KEY`,
      },
    });
  }
}
